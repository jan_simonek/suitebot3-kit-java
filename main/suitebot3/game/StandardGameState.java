package suitebot3.game;

import suitebot3.game.dto.GameStateDTO;

public class StandardGameState implements GameState
{
	protected int roundsRemaining;
	protected int currentRound;

	public StandardGameState()
	{
	}

	@Override
	public int getCurrentRound()
	{
		return currentRound;
	}

	@Override
	public int getRoundsRemaining()
	{
		return roundsRemaining;
	}

	@Override
	public GameStateDTO toDto()
	{
		return new GameStateDTO();
	}
}
