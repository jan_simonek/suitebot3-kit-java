package suitebot3;

import suitebot3.ai.BotAiFactory;
import suitebot3.ai.SampleBotAi;
import suitebot3.server.SimpleServer;

public class BotServer
{
	private static final int DEFAULT_PORT = 9001;

	public static void main(String[] args)
	{
		int port = determinePort(args);
		BotAiFactory botAiFactory = SampleBotAi::new; // replace with your own implementation of BotAi

		startBot(botAiFactory, port);
	}

	public static void startBot(BotAiFactory botAiFactory, int port)
	{
		System.out.println("listening on port " + port);

		new SimpleServer(port, new BotRequestHandler(botAiFactory))
				.run();
	}

	private static int determinePort(String[] args)
	{
		if (args.length == 1)
			return Integer.valueOf(args[0]);
		else
			return DEFAULT_PORT;
	}
}
