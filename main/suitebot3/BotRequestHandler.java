package suitebot3;

import suitebot3.ai.BotAi;
import suitebot3.ai.BotAiFactory;
import suitebot3.game.GameSetup;
import suitebot3.game.GameStateCreator;
import suitebot3.game.Moves;
import suitebot3.game.dto.GameStateDTO;
import suitebot3.game.dto.JsonUtil;
import suitebot3.server.SimpleRequestHandler;

public class BotRequestHandler implements SimpleRequestHandler
{
	private final BotAiFactory botAiFactory;
	private BotAi botAi;
	private boolean suppressErrorLogging = false;

	public BotRequestHandler(BotAiFactory botAiFactory)
	{
		this.botAiFactory = botAiFactory;
	}

	@Override
	public String processRequest(String request)
	{
		try
		{
			return processRequestInternal(request).serialize();
		}
		catch (Exception e)
		{
			if (!suppressErrorLogging)
			{
				System.err.println("error while processing the request: " + request);
				e.printStackTrace();
			}

			return e.toString();
		}
	}

	private Moves processRequestInternal(String request)
	{
		GameStateDTO gameStateDto = JsonUtil.decodeGameState(request);

		if (gameStateDto.currentRound == 1)
			botAi = botAiFactory.createNewBot(GameSetup.fromGameStateDto(gameStateDto));

		return new Moves();
	}

	public void setSuppressErrorLogging(boolean suppressErrorLogging)
	{
		this.suppressErrorLogging = suppressErrorLogging;
	}
}
