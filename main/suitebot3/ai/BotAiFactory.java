package suitebot3.ai;

import suitebot3.game.GameSetup;

public interface BotAiFactory
{
    BotAi createNewBot(GameSetup gameSetup);
}
