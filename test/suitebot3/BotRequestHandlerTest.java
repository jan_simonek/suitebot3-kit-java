package suitebot3;

import com.google.gson.Gson;
import org.junit.Before;
import org.junit.Test;
import suitebot3.ai.BotAi;
import suitebot3.game.Action;
import suitebot3.game.GameState;
import suitebot3.game.GameStateCreator;
import suitebot3.game.dto.GameStateDTO;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class BotRequestHandlerTest
{
    private BotRequestHandler requestHandler;

    @Before
    public void setUp()
    {
        requestHandler = new BotRequestHandler(gameSetup -> new DummyBotAi());
    }

    @Test
    public void onInvalidRequest_noAiMethodsShouldBeCalled()
    {
        requestHandler.setSuppressErrorLogging(true);
        requestHandler.processRequest("invalid request");
    }

    class DummyBotAi implements BotAi
    {
        @Override
        public Action makeMove(GameState gameState)
        {
            if (gameState.getCurrentRound() == 1)
                return null;
            else if (gameState.getCurrentRound() == 2)
                return null;
            else
                throw new RuntimeException("unexpected round");
        }

    }
}
